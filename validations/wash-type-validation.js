const yup = require('yup');

const washTypeValidation = yup.object({
  body: yup.object({
    type: yup.string().required('Wash type is required'),
  }),
});

module.exports = {
    washTypeValidation
};
