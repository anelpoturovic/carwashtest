const Statistcs = require('../models/statistics-model');

const getAllStats = async (req, res, next) => {
  try {
    const { dateFrom, dateUpto } = req.query;
    const _dateFrom = new Date(dateFrom);
    const _dateUpto = new Date(dateUpto);
    _dateUpto.setHours(_dateUpto.getHours() + 23, 59, 59);

    const statistics = await _getStats({
      createdAt: { $gte: _dateFrom, $lte: _dateUpto },
    });
    res.send({ message: 'Success', statistics});
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const getUnrecommendedStats = async (req, res, next) => {
  try {
    const { dateFrom, dateUpto } = req.query;
    const _dateFrom = new Date(dateFrom);
    const _dateUpto = new Date(dateUpto);
    _dateUpto.setHours(_dateUpto.getHours() + 23, 59, 59);

    const statistics = await _getStats({
      createdAt: { $gte: _dateFrom, $lte: _dateUpto },
      washType: { $ne: 'contract' },
    });
    res.send({ message: 'Success', statistics });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const _getStats = async (match) => {
  const statistics = await Statistcs.aggregate([
    {
      $match: match,
    },
    {
      $group: {
        _id: {
          facility: '$facility',
          washType: '$washType',
        },
        count: { $sum: 1 },
      },
    },
  ]).exec();
  return statistics;
};

module.exports = {
  getAllStats,
  getUnrecommendedStats,
};
