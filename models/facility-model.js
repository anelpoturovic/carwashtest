const mongoose = require('mongoose');

const { Schema } = mongoose;

const facilitySchema = Schema(
  {
    name: {
      required: true,
      type: String,
      trim: true,
    },
  },

  { timestamps: true }
);

// facilitySchema.pre('save', function (next) {
//   const currentDate = new Date();
//   this.updated_at = currentDate;

//   if (!this.created_at) this.created_at = currentDate;

//   let facilityId = 1;
//   const facility = this;
//   Facility.find({}, function (err, facilities) {
//     if (err) throw err;
//     facilityId = facility.length + 1;
//     user.sno = sno;
//     next();
//   });
// });

const Facility = mongoose.model('Facility', facilitySchema);

module.exports = Facility;
