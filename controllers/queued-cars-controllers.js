const CarInQueue = require('../models/queued-cars-model');
const Statistics = require('../models/statistics-model');
const WashType = require('../models/wash-type-model');
const Facility = require('../models/facility-model');
const mongoose = require('mongoose');

const addCarInqueue = async (req, res, next) => {
  try {
    const { customer, washType, facility } = req.body;
    const session = await mongoose.startSession();
    const carInqueue = new CarInQueue({ customer, washType, facility });

    const _washType = await WashType.findById(washType);
    const _facility = await Facility.findById(facility);

    const statistic = new Statistics({
      washType: _washType.type,
      facility: _facility.name,
    });
    await session.withTransaction(async () => {
      await carInqueue.save();
      await statistic.save();
    });
    session.endSession();

    res.status(201).send({ message: 'Success', carInqueue });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const deleteCarInQueue = async (req, res, next) => {
  try {
    const { carInQueueId } = req.params;
    await CarInQueue.findByIdAndDelete(carInQueueId);
    res.status(204).send({ message: 'Success' });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const getCarInqueue = async (req, res, next) => {
  try {
    const { carInQueueId } = req.params;
    const car = await CarInQueue.findById(carInQueueId)
      .populate('facility')
      .populate('customer')
      .populate('washType');
    res.send({ message: 'Success', car });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const getCarsInqueue = async (req, res, next) => {
  try {
    const { limit = 10, page = 1 } = req.query;
    const totalCarsInqueue = await CarInQueue.find().countDocuments();
    const cars = await CarInQueue.find()
      .skip((+page - 1) * +limit)
      .limit(+limit)
      .populate('facility')
      .populate('customer')
      .populate('washType');
    res.send({ message: 'Success', cars, totalCarsInqueue });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

module.exports = {
  addCarInqueue,
  deleteCarInQueue,
  getCarInqueue,
  getCarsInqueue,
};
