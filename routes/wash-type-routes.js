const express = require('express');

const router = express.Router();

const { validate } = require('../middlewares/validate');
const { washTypeValidation } = require('../validations/wash-type-validation');

const {
  addWashType,
  updateWashType,
  deleteWashType,
  getWashTypes,
  getWashType,
} = require('../controllers/wash-type-controllers');

// Add washType
router.post('/add', validate(washTypeValidation), addWashType);

// Update washType
router.patch(
  '/:washTypeId',
  validate(washTypeValidation),
  updateWashType
);

// Delete washType
router.delete('/:washTypeId', deleteWashType);

// Get All facilities
router.get('/get', getWashTypes);

// Get single washType
router.get('/:washTypeId', getWashType);



module.exports = router;
