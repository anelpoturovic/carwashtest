const express = require('express');

const router = express.Router();

const { validate } = require('../middlewares/validate');
const { queuedCarsValidation } = require('../validations/queued-cars-validation');

const {
  addCarInqueue,
  getCarInqueue,
  getCarsInqueue,
  deleteCarInQueue,
} = require('../controllers/queued-cars-controllers');

// Add car in queue
router.post('/add', validate(queuedCarsValidation), addCarInqueue);


// Delete car in queue
router.delete('/:carInQueueId', deleteCarInQueue);

// Get All Cars in queue
router.get('/get', getCarsInqueue);

// Get single car in queue
router.get('/:carInQueueId', getCarInqueue);

module.exports = router;
