const express = require('express');

const router = express.Router();

const { getAllStats,getUnrecommendedStats } = require('../controllers/statistics-controllers');

// Get all stats
router.get('/getAll', getAllStats);
router.get('/getUnrecommended', getUnrecommendedStats);

module.exports = router;
