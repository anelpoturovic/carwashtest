const mongoose = require("mongoose");

const connectApp = function () {
  return mongoose.connect(process.env.MONGODB_URL);
};

module.exports = connectApp;
