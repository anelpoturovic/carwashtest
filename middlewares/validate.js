const validate = (schema) => async (req, res, next) => {

    try {
      await schema.validate({
        body: req.body,
        // query: req.query,
        // params: req.params,
      });
      return next();
    } catch (err) {
        console.log(err.name,err.message)
        return res.status(500).send({ type: err.name, message: err.message })

        // const err = new Error()
        // err.message = error.message
        // err.status = 500
        // next(err)
    }
  };
  

  module.exports = {
      validate
  }