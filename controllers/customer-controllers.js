const Customer = require('../models/customer-model');
const WashType = require('../models/wash-type-model');
const { getValidUpdates } = require('../utils/valid-updates');

const addCustomer = async (req, res, next) => {
  try {
    const { name, carPlate, agreementType, facility } = req.body;

    const customer = await Customer.findOneAndUpdate(
      { carPlate },
      { name, agreementType, facility },
      { upsert: true, new: true }
    );

    let recommendedWashType = null;
    if (customer.agreementType) {
      recommendedWashType = await WashType.findOne({ type: 'contract' });
    }
    // const customer = new Customer({name,carPlate,agreementType,facility})
    // await customer.save()
    res.status(201).send({ message: 'Success', customer, recommendedWashType });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const updateCustomer = async (req, res, next) => {
  try {
    const { customerId } = req.params;
    console.log(customerId)
    const updates = getValidUpdates(req.body, [
      'name',
      'carPlate',
      'agreementType',
      'facility',
    ]);
    const customer = await Customer.findById(customerId);
    for (let update in updates) {
      customer[update] = updates[update];
    }
    await customer.save();

    res.send({ message: 'Success', customer });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const deleteCustomer = async (req, res, next) => {
  try {
    const { customerId } = req.params;
    await Customer.findByIdAndDelete(customerId);
    res.status(204).send({ message: 'Success' });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const getCustomer = async (req, res, next) => {
  try {
    const { customerId } = req.params;
    const customer = await Customer.findById(customerId);
    res.send({ message: 'Success',customer });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const getCustomers = async (req, res, next) => {
  try {
    const { limit = 10, page = 1 } = req.query;
    const customers = await Customer.find()
      .skip((+page - 1) * +limit)
      .limit(+limit);
    res.send({ message: 'Success', customers });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

module.exports = {
  addCustomer,
  updateCustomer,
  deleteCustomer,
  getCustomer,
  getCustomers,
};
