const Facility = require('../models/facility-model');
const { getValidUpdates } = require('../utils/valid-updates');

const addFacility = async (req, res, next) => {
  try {
    const { name } = req.body;
    const facility = new Facility({ name });
    await facility.save();
    res.status(201).send({ message: 'Success', facility });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const updateFacility = async (req, res, next) => {
  try {
    console.log(req.params);
    const { facilityId } = req.params;

    const updates = getValidUpdates(req.body, ['name']);
    const facility = await Facility.findById(facilityId);
    if (!facility) {
      const err = new Error('Facility not found');
      err.status = 404;
      return next(err);
    }
    for (let update in updates) {
      facility[update] = updates[update];
    }
    await facility.save();

    res.send({ message: 'Success', facility });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};
const deleteFacility = async (req, res, next) => {
  try {
    const { facilityId } = req.params;
    await Facility.findByIdAndDelete(facilityId);
    res.status(204).send({ message: 'Success' });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const getFacility = async (req, res, next) => {
  try {
    const { facilityId } = req.params;
    console.log(facilityId);
    const facility = await Facility.findById(facilityId);
    res.send({ message: 'Success', facility });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const getFacilities = async (req, res, next) => {
  try {
    const { limit = 10, page = 1 } = req.query;
    const facilities = await Facility.find()
      .skip((+page - 1) * +limit)
      .limit(+limit);
    res.send({ message: 'Success', facilities });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

module.exports = {
  addFacility,
  updateFacility,
  deleteFacility,
  getFacility,
  getFacilities,
};
