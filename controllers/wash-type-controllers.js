const WashType = require('../models/wash-type-model');
const { getValidUpdates } = require('../utils/valid-updates');

const addWashType = async (req, res, next) => {
  try {
    const { type } = req.body;
    const washType = new WashType({ type });
    await washType.save();
    res.status(201).send({ message: 'Success', washType });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const updateWashType = async (req, res, next) => {
  try {
    const { washTypeId } = req.params;

    const updates = getValidUpdates(req.body, ['type']);
    const washType = await WashType.findById(washTypeId);
    if (!washType) {
      const err = new Error('Wash type not found');
      err.status = 404;
      return next(err);
    }
    for (let update in updates) {
      washType[update] = updates[update];
    }
    await washType.save();

    res.send({ message: 'Success', washType });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const deleteWashType = async (req, res, next) => {
  try {
    const { washTypeId } = req.params;
    await WashType.findByIdAndDelete(washTypeId);
    res.status(204).send({ message: 'Success' });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const getWashType = async (req, res, next) => {
  try {
    const { washTypeId } = req.params;
     const washType = await WashType.findById(washTypeId);
    res.send({ message: 'Success',washType });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

const getWashTypes = async (req, res, next) => {
  try {
    const { limit = 10, page = 1 } = req.query;
    const facilities = await WashType.find()
      .skip((+page - 1) * +limit)
      .limit(+limit);
    res.send({ message: 'Success', facilities });
  } catch (error) {
    const err = new Error();
    err.message = error.message;
    err.status = 500;
    next(err);
  }
};

module.exports = {
  addWashType,
  updateWashType,
  deleteWashType,
  getWashType,
  getWashTypes,
};
