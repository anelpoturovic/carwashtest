{
	"info": {
		"_postman_id": "b069db5c-258b-46d6-9532-5eca53a87472",
		"name": "car-wash",
		"description": "In order to test the API please create at least one document in the following collections, because other collections depend on these two:\n\n1)Create at least 1 facility\n2)Create at least 1 wash type\n\nAfter that you need to create a customer\n\nAfter you have created customer you can now add the car/customer in queue so now you can add document to car-queue collection\n\nFor further information see requests and their examples down below",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
	},
	"item": [
		{
			"name": "facility",
			"item": [
				{
					"name": "Add facility",
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"name\":\"facility1\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{baseUrl}}/facility/add",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"facility",
								"add"
							]
						}
					},
					"response": [
						{
							"name": "Add facility",
							"originalRequest": {
								"method": "POST",
								"header": [],
								"body": {
									"mode": "raw",
									"raw": "{\r\n    \"name\":\"facility2\"\r\n}",
									"options": {
										"raw": {
											"language": "json"
										}
									}
								},
								"url": {
									"raw": "{{baseUrl}}/facility/add",
									"host": [
										"{{baseUrl}}"
									],
									"path": [
										"facility",
										"add"
									]
								}
							},
							"status": "Created",
							"code": 201,
							"_postman_previewlanguage": "json",
							"header": [
								{
									"key": "X-Powered-By",
									"value": "Express"
								},
								{
									"key": "Content-Type",
									"value": "application/json; charset=utf-8"
								},
								{
									"key": "Content-Length",
									"value": "172"
								},
								{
									"key": "ETag",
									"value": "W/\"ac-6CPD8pk3ijkrTzHBtnKZbkEmF+s\""
								},
								{
									"key": "Date",
									"value": "Sat, 30 Oct 2021 19:23:11 GMT"
								},
								{
									"key": "Connection",
									"value": "keep-alive"
								}
							],
							"cookie": [],
							"body": "{\n    \"message\": \"Success\",\n    \"facility\": {\n        \"name\": \"facility2\",\n        \"_id\": \"617d9b9fd5b83b9611ddad5c\",\n        \"createdAt\": \"2021-10-30T19:23:11.599Z\",\n        \"updatedAt\": \"2021-10-30T19:23:11.599Z\",\n        \"__v\": 0\n    }\n}"
						}
					]
				},
				{
					"name": "Get facility",
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "{{baseUrl}}/facility/{facilityId}",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"facility",
								"{facilityId}"
							]
						}
					},
					"response": [
						{
							"name": "Get facility",
							"originalRequest": {
								"method": "GET",
								"header": [],
								"url": {
									"raw": "{{baseUrl}}/facility/617db56d4912ff1efed85c1f",
									"host": [
										"{{baseUrl}}"
									],
									"path": [
										"facility",
										"617db56d4912ff1efed85c1f"
									]
								}
							},
							"status": "OK",
							"code": 200,
							"_postman_previewlanguage": "json",
							"header": null,
							"cookie": [],
							"body": "{\n    \"message\": \"Success\",\n    \"facility\": {\n        \"_id\": \"617db56d4912ff1efed85c1f\",\n        \"name\": \"facility1\",\n        \"createdAt\": \"2021-10-30T21:13:17.728Z\",\n        \"updatedAt\": \"2021-10-30T21:13:17.728Z\",\n        \"__v\": 0\n    }\n}"
						}
					]
				},
				{
					"name": "Update facility",
					"request": {
						"method": "PATCH",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"name\":\"facility1 updated\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{baseUrl}}/facility/617db56d4912ff1efed85c1f",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"facility",
								"617db56d4912ff1efed85c1f"
							]
						}
					},
					"response": [
						{
							"name": "Update facility",
							"originalRequest": {
								"method": "PATCH",
								"header": [],
								"body": {
									"mode": "raw",
									"raw": "{\r\n    \"name\":\"facility1 updated\"\r\n}",
									"options": {
										"raw": {
											"language": "json"
										}
									}
								},
								"url": {
									"raw": "{{baseUrl}}/facility/617db56d4912ff1efed85c1f",
									"host": [
										"{{baseUrl}}"
									],
									"path": [
										"facility",
										"617db56d4912ff1efed85c1f"
									]
								}
							},
							"_postman_previewlanguage": "json",
							"header": null,
							"cookie": [],
							"body": "{\n    \"message\": \"Success\",\n    \"facility\": {\n        \"_id\": \"617db56d4912ff1efed85c1f\",\n        \"name\": \"facility1 updated\",\n        \"createdAt\": \"2021-10-30T21:13:17.728Z\",\n        \"updatedAt\": \"2021-10-30T21:16:43.774Z\",\n        \"__v\": 0\n    }\n}"
						}
					]
				},
				{
					"name": "Get all facilities",
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "{{baseUrl}}/facility/get",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"facility",
								"get"
							]
						}
					},
					"response": [
						{
							"name": "Get all facilities",
							"originalRequest": {
								"method": "GET",
								"header": [],
								"url": {
									"raw": "{{baseUrl}}/facility/get",
									"host": [
										"{{baseUrl}}"
									],
									"path": [
										"facility",
										"get"
									]
								}
							},
							"_postman_previewlanguage": "json",
							"header": null,
							"cookie": [],
							"body": "{\n    \"message\": \"Success\",\n    \"facilities\": [\n        {\n            \"_id\": \"617db56d4912ff1efed85c1f\",\n            \"name\": \"facility1 updated\",\n            \"createdAt\": \"2021-10-30T21:13:17.728Z\",\n            \"updatedAt\": \"2021-10-30T21:16:43.774Z\",\n            \"__v\": 0\n        }\n    ]\n}"
						}
					]
				},
				{
					"name": "Delete facility",
					"request": {
						"method": "DELETE",
						"header": [],
						"url": {
							"raw": "{{baseUrl}}/facility/{facilityId}",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"facility",
								"{facilityId}"
							]
						}
					},
					"response": [
						{
							"name": "Delete facility",
							"originalRequest": {
								"method": "DELETE",
								"header": [],
								"url": {
									"raw": "{{baseUrl}}/facility/617d0cbdb7e9bd64bd125e82",
									"host": [
										"{{baseUrl}}"
									],
									"path": [
										"facility",
										"617d0cbdb7e9bd64bd125e82"
									]
								}
							},
							"status": "No Content",
							"code": 204,
							"_postman_previewlanguage": null,
							"header": null,
							"cookie": [],
							"body": null
						}
					]
				}
			]
		},
		{
			"name": "customer",
			"item": [
				{
					"name": "Add customer",
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "formdata",
							"formdata": [
								{
									"key": "name",
									"value": "customer1",
									"type": "text"
								},
								{
									"key": "carPlate",
									"value": "aaa-000",
									"type": "text"
								},
								{
									"key": "facility",
									"value": "617db56d4912ff1efed85c1f",
									"type": "text"
								},
								{
									"key": "agreementType",
									"value": "business",
									"description": "optional",
									"type": "text"
								}
							]
						},
						"url": {
							"raw": "{{baseUrl}}/customer/add",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"customer",
								"add"
							]
						}
					},
					"response": [
						{
							"name": "Add customer",
							"originalRequest": {
								"method": "POST",
								"header": [],
								"body": {
									"mode": "formdata",
									"formdata": [
										{
											"key": "name",
											"value": "customer1",
											"type": "text"
										},
										{
											"key": "carPlate",
											"value": "aaa-000",
											"type": "text"
										},
										{
											"key": "facility",
											"value": "617db56d4912ff1efed85c1f",
											"type": "text"
										},
										{
											"key": "agreementType",
											"value": "business",
											"description": "optional",
											"type": "text"
										}
									],
									"options": {
										"raw": {
											"language": "json"
										}
									}
								},
								"url": {
									"raw": "{{baseUrl}}/customer/add",
									"host": [
										"{{baseUrl}}"
									],
									"path": [
										"customer",
										"add"
									]
								}
							},
							"status": "Created",
							"code": 201,
							"_postman_previewlanguage": "json",
							"header": [
								{
									"key": "X-Powered-By",
									"value": "Express"
								},
								{
									"key": "Content-Type",
									"value": "application/json; charset=utf-8"
								},
								{
									"key": "Content-Length",
									"value": "258"
								},
								{
									"key": "ETag",
									"value": "W/\"102-UT7yG5nB5vKweEnpjSevjTGoq08\""
								},
								{
									"key": "Date",
									"value": "Sat, 30 Oct 2021 21:24:50 GMT"
								},
								{
									"key": "Connection",
									"value": "keep-alive"
								}
							],
							"cookie": [],
							"body": "{\n    \"message\": \"Success\",\n    \"customer\": {\n        \"_id\": \"617db822c07340f67dbfbbcc\",\n        \"carPlate\": \"aaa-000\",\n        \"__v\": 0,\n        \"createdAt\": \"2021-10-30T21:24:50.323Z\",\n        \"facility\": \"617db56d4912ff1efed85c1f\",\n        \"name\": \"customer1\",\n        \"updatedAt\": \"2021-10-30T21:24:50.323Z\"\n    },\n    \"recommendedWashType\": null\n}"
						}
					]
				},
				{
					"name": "Get customer",
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "{{baseUrl}}/customer/617d61b7c07340f67d8680a6",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"customer",
								"617d61b7c07340f67d8680a6"
							]
						}
					},
					"response": []
				},
				{
					"name": "Update customer",
					"request": {
						"method": "PATCH",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"agreementType\":\"premium\"\r\n   \r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{baseUrl}}/customer/617d26eac07340f67d61f319",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"customer",
								"617d26eac07340f67d61f319"
							]
						}
					},
					"response": []
				},
				{
					"name": "Get all customers",
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "{{baseUrl}}/customer/get",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"customer",
								"get"
							]
						}
					},
					"response": []
				},
				{
					"name": "Delete customer",
					"request": {
						"method": "DELETE",
						"header": [],
						"url": {
							"raw": "{{baseUrl}}/customer/617d2650c07340f67d6195c2",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"customer",
								"617d2650c07340f67d6195c2"
							]
						}
					},
					"response": []
				}
			]
		},
		{
			"name": "wash-type",
			"item": [
				{
					"name": "Add wash type",
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"type\":\"Contract\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{baseUrl}}/washType/add",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"washType",
								"add"
							]
						}
					},
					"response": []
				},
				{
					"name": "Delete wash type",
					"request": {
						"method": "DELETE",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"type\":\"test\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{baseUrl}}/washType/617d1eadaecfce38359abcb1",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"washType",
								"617d1eadaecfce38359abcb1"
							]
						}
					},
					"response": []
				},
				{
					"name": "Update wash type",
					"request": {
						"method": "PATCH",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"type\":\"gold\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{baseUrl}}/washType/617d1eadaecfce38359abcb1",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"washType",
								"617d1eadaecfce38359abcb1"
							]
						}
					},
					"response": []
				},
				{
					"name": "Get all wash types",
					"protocolProfileBehavior": {
						"disableBodyPruning": true
					},
					"request": {
						"method": "GET",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"type\":\"gold\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{baseUrl}}/washType/get",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"washType",
								"get"
							]
						}
					},
					"response": []
				},
				{
					"name": "Get wash type",
					"protocolProfileBehavior": {
						"disableBodyPruning": true
					},
					"request": {
						"method": "GET",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"type\":\"gold\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{baseUrl}}/washType/617d1ecf5a43b057963997f0",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"washType",
								"617d1ecf5a43b057963997f0"
							]
						}
					},
					"response": []
				}
			]
		},
		{
			"name": "car-queue",
			"item": [
				{
					"name": "Add car in queue",
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"customer\":\"617d61b7c07340f67d8680a6\",\r\n    \"facility\":\"617d9b9fd5b83b9611ddad5c\",\r\n    \"washType\":\"617d26d7848ade5ed363c0a2\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{baseUrl}}/queuedCars/add",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"queuedCars",
								"add"
							]
						}
					},
					"response": []
				},
				{
					"name": "Delete car in queue",
					"request": {
						"method": "DELETE",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"type\":\"test\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{baseUrl}}/queuedCars/617d8f2ded280efe41bffb6c",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"queuedCars",
								"617d8f2ded280efe41bffb6c"
							]
						}
					},
					"response": []
				},
				{
					"name": "Get all cars in queue",
					"protocolProfileBehavior": {
						"disableBodyPruning": true
					},
					"request": {
						"method": "GET",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"type\":\"gold\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{baseUrl}}/queuedCars/get",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"queuedCars",
								"get"
							]
						}
					},
					"response": []
				},
				{
					"name": "Get car in queue",
					"protocolProfileBehavior": {
						"disableBodyPruning": true
					},
					"request": {
						"method": "GET",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"type\":\"gold\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "{{baseUrl}}/queuedCars/617d7a6899eacd66564c6d2e",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"queuedCars",
								"617d7a6899eacd66564c6d2e"
							]
						}
					},
					"response": []
				}
			]
		},
		{
			"name": "statistics",
			"item": [
				{
					"name": "Get all statistics",
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "{{baseUrl}}/stats/getAll?dateFrom=2021-10-30&dateUpto=2021-10-30",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"stats",
								"getAll"
							],
							"query": [
								{
									"key": "dateFrom",
									"value": "2021-10-30"
								},
								{
									"key": "dateUpto",
									"value": "2021-10-30"
								}
							]
						}
					},
					"response": []
				},
				{
					"name": "New Request",
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "{{baseUrl}}/stats/getUnrecommended?dateFrom=2021-10-30&dateUpto=2021-10-30",
							"host": [
								"{{baseUrl}}"
							],
							"path": [
								"stats",
								"getUnrecommended"
							],
							"query": [
								{
									"key": "dateFrom",
									"value": "2021-10-30"
								},
								{
									"key": "dateUpto",
									"value": "2021-10-30"
								}
							]
						}
					},
					"response": []
				}
			]
		}
	]
}