const express = require('express');

const router = express.Router();

const { validate } = require('../middlewares/validate');
const { facilityValidation } = require('../validations/facility-validation');

const {
  addFacility,
  updateFacility,
  deleteFacility,
  getFacilities,
  getFacility,
} = require('../controllers/facility-controllers');

// Add facility
router.post('/add', validate(facilityValidation), addFacility);

// Update facility
router.patch(
  '/:facilityId',
  validate(facilityValidation),
  updateFacility
);

// Delete facility
router.delete('/:facilityId', deleteFacility);

// Get All facilities
router.get('/get', getFacilities);

// Get single facility
router.get('/:facilityId', getFacility);



module.exports = router;
