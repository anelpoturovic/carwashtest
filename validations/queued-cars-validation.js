const mongoose = require('mongoose');

const yup = require('yup');

const queuedCarsValidation = yup.object({
  body: yup.object({
    customer: yup
      .string()
      .required('customer id is required')
      .test('is-mongoose-id', '${path} is not an objectId', (value, context) =>
        mongoose.Types.ObjectId.isValid(value)
      ),
    facility: yup
      .string()
      .required('facility id is required')
      .test('is-mongoose-id', '${path} is not an objectId', (value, context) =>
        mongoose.Types.ObjectId.isValid(value)
      ),
    washType: yup
      .string()
      .required('washType id is required')
      .test('is-mongoose-id', '${path} is not an objectId', (value, context) =>
        mongoose.Types.ObjectId.isValid(value)
      ),
  }),
});

module.exports = {
    queuedCarsValidation,
};
