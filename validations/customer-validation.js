const mongoose = require('mongoose');

const yup = require('yup');

const customerValidation = yup.object({
  body: yup.object({
    name: yup.string().required('Customer name is required'),
    carPlate: yup.string().required('Car plate is required'),
    agreementType: yup
      .string()
      .oneOf(['business', 'premium']),
    facility: yup
      .string()
      .required('facility id is required')
      .test('is-mongoose-id', '${path} is not an objectId', (value, context) =>
        mongoose.Types.ObjectId.isValid(value)
      ),
  }),
});

module.exports = {
  customerValidation,
};
