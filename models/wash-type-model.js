const mongoose = require('mongoose');

const { Schema } = mongoose;

const washTypeSchema = Schema(
  {
    type: {
      required: true,
      type: String,
      trim: true,
      unique:true,
      lowercase:true,
      enum:['contract','gold','normal']
    },
  },

  { timestamps: true }
);

const WashType = mongoose.model('WashType', washTypeSchema);

module.exports = WashType;
