const yup = require('yup');

const facilityValidation = yup.object({
  body: yup.object({
    name: yup.string().required('Facility name is required'),
  }),
});

module.exports = {
  facilityValidation
};
