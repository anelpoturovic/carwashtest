const express = require('express');

const router = express.Router();

const { validate } = require('../middlewares/validate');
const { customerValidation } = require('../validations/customer-validation');

const {
  addCustomer,
  updateCustomer,
  deleteCustomer,
  getCustomers,
  getCustomer,
} = require('../controllers/customer-controllers');

// Add customer
router.post('/add', validate(customerValidation), addCustomer);

// Update customer
router.patch('/:customerId', updateCustomer);

// Delete customer
router.delete('/:customerId', deleteCustomer);

// Get All facilities
router.get('/get', getCustomers);

// Get single customer
router.get('/:customerId', getCustomer);

module.exports = router;
