const mongoose = require('mongoose')
// Installed modules
const path = require('path');



const express = require('express');
// const cors = require('cors');

// dotenv config
require('dotenv').config({
  path: path.join(__dirname, `/config/${process.env.NODE_ENV}.env`),
});

// Project Modules
const dbConnection = require('./config/db');

const facilityRoutes = require('./routes/facility-routes');
const washTypeRoutes = require('./routes/wash-type-routes');
const customerRoutes = require('./routes/customer-routes');
const queuedCarsRoutes = require('./routes/queued-cars-routes');
const statisticsRoutes = require('./routes/statistics-routes');

// init express
const app = express();

// Allowing cross origin requests
// app.use(cors({ origin: 'http://localhost:8080', credentials: true }));

// Automatically parsing incoming json to object
app.use(express.json());

//accepting form data
app.use(express.urlencoded({ extended: false }));

// App Routes
app.use('/api/facility', facilityRoutes);
app.use('/api/washType', washTypeRoutes);
app.use('/api/customer', customerRoutes);
app.use('/api/queuedCars', queuedCarsRoutes);
app.use('/api/stats', statisticsRoutes);

// Error handler
app.use((err, req, res, next) => {
  const status = err.status || 500;
  const message = err.message || 'Oops, an error occured';
  res.status(status).send({ message });
});

const port = process.env.PORT || 3000;

// Server
dbConnection()
  .then(async () => {
    app.listen(port, () => console.log('Server is up on port: ' + port));
    // await mongoose.connection.db.dropDatabase();
  })
  .catch((err) => {
    console.log(err);
  });
