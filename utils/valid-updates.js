const getValidUpdates = (updates, allowedUpdates) => {
  const validUpdates = {};
  const updatesKeys = Object.keys(updates);
  updatesKeys.forEach((update) => {
    if (allowedUpdates.indexOf(update) > -1) {
      validUpdates[update] = updates[update];
    }
  });

  return validUpdates;
};

module.exports = { getValidUpdates };
