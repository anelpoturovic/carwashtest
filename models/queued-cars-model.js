const mongoose = require('mongoose');

const { Schema } = mongoose;

const queuedCarsSchema = Schema(
  {
    customer: {
      required: true,
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Customer',
      unique:true
    },
    washType: {
      required: true,
      type: mongoose.Schema.Types.ObjectId,
      ref: 'WashType',
    },
    facility: {
      required: true,
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Facility',
    },
  },

  { timestamps: true }
);



const QueuedCars = mongoose.model('QueuedCars', queuedCarsSchema);

module.exports = QueuedCars;
