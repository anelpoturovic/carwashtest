const mongoose = require('mongoose');

const { Schema } = mongoose;

const customerSchema = Schema(
  {
    name: {
      required: true,
      type: String,
      trim: true,
    },
    carPlate: {
      required: true,
      type: String,
      trim: true,
      unique:true
    },
    agreementType: {
      type: String,
      enum: ['business', 'premium'],
    },
    facility: {
      type: Schema.Types.ObjectId,
      ref: 'Facility',
      required: true,
    },
  },

  { timestamps: true }
);

const Customer = mongoose.model('Customer', customerSchema);

module.exports = Customer;
