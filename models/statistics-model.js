const mongoose = require('mongoose');

const { Schema } = mongoose;

const statisticsSchema = Schema(
  {
    washType: {
      required: true,
      type: String,

    },
    facility: {
      required: true,
      type: String,
    },
    recommended: {
      type: Boolean,
    },
  },

  { timestamps: true }
);

statisticsSchema.pre('save', function (next) {
    console.log(this.washType)
  this.recommended = this.washType.toLowerCase() === 'contract';
  next();
});

const Statistics = mongoose.model('Statistics', statisticsSchema);

module.exports = Statistics;
